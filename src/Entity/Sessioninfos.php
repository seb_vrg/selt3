<?php

namespace App\Entity;

use App\Entity\User;
use App\Entity\Userslist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table(name="sessioninfos")
 * @ORM\Entity(repositoryClass="App\Repository\SessioninfosRepository")
 */
class Sessioninfos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id",type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="mailsent",type="boolean",nullable=true)
     */
    private $mailsent;

    /**
     * @ORM\Column(name="isdone",type="boolean",nullable=true)
     */
    private $isdone;

    /**
     * @var Studentgroup
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Studentgroup",inversedBy="sessioninfos")
     */
    private $studentgroup;

    /**
     * @var User
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\User",inversedBy="sessioninfos")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMailsent(): ?bool
    {
        return $this->mailsent;
    }

    public function setMailsent(bool $mailsent): self
    {
        $this->mailsent = $mailsent;

        return $this;
    }

    public function getIsdone(): ?bool
    {
        return $this->isdone;
    }

    public function setIsdone(bool $isdone): self
    {
        $this->isdone = $isdone;

        return $this;
    }


}
