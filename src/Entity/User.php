<?php

namespace App\Entity;

use App\Entity\Repax;
use App\Entity\Company;
use App\Entity\Userslist;
use App\Entity\Sessioninfos;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\Common\Collections\ArrayCollection;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(
 *  fields={"email"},
 *  message="L'email indiqué est déjà utilisé !"
 * )
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     * @Assert\Length(min="8", minMessage="Votre mot de passe doit faire minimum 8 caractères")
     * 
     */
    private $password;

    /**
     * @Assert\EqualTo(propertyPath="password", message="Vous n'avez pas tapé le même mot de passe")
     */
    public $confirm_password;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="array", nullable=false)
     */
    private $roles = [];

    /**
     * @var bool
     *
     * @ORM\Column(name="isreferant", type="boolean", nullable=true)
     */
    private $isreferant;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="isstudent", type="boolean", nullable=true)
     */
    private $isstudent;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Company",inversedBy="user")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var Userslist
     * 
     * @ORM\ManyToMany(targetEntity="App\Entity\Userslist",cascade={"persist","remove"})
     * @JoinTable(name="list_de_stagiaire",
     *      joinColumns={@JoinColumn(name="user_id",referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="userslist_id",referencedColumnName="id")}
     * )
     */
    private $userslist;


    /**
     * @var Repax
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\Repax",mappedBy="user")
     */
    private $repax;

    /**
     * @var Sessioninfos
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\Sessioninfos",mappedBy="user")
     */
    private $sessioninfos;

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nom;
    }

    public function __construct()
    {
        $this->userslist = new ArrayCollection();
        $this->repax = new ArrayCollection();
        $this->sessioninfos = new ArrayCollection();
    }
     /**
     * Get id
     *
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSalt()
    {
      
    }
    public function eraseCredentials()
    {
    }
    
    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
   
    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getRoles()
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';
        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getIsreferant(): ?bool
    {
        return $this->isreferant;
    }

    public function setIsreferant(?bool $isreferant): self
    {
        $this->isreferant = $isreferant;

        return $this;
    }

    public function getIsstudent(): ?bool
    {
        return $this->isstudent;
    }

    public function setIsstudent(?bool $isstudent): self
    {
        $this->isstudent = $isstudent;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Add userslist
     * 
     * @param Userslist $userslist
     * @return $this
     */
    public function addUserslist(Userslist $userslist)
    {
        $this->userslist[]= $userslist;

        return $this;
    }

    /**
     * Remove userslist
     */
    public function removeUserslist(\App\Entity\Userslist $userslist)
    {
        $this->userslist->removeElement($userslist);

    }

    /**
     * Get userslist
     * 
     * @return Collection|Userslist[]
     */
    public function getUserslist(): array
    {
        return $this->userslist;
    }

    public function addRepax(Repax $repax)
    {
        $this->repax[] = $repax;
    }

    public function removeRepax(Repax $repax)
    {
        $this->repax->removeElement($repax);
    }

    public function getRepax()
    {
        return $this->repax;
    }

    public function addSessioninfos(Sessioninfos $sessioninfos)
    {
        $this->sessioninfos[] = $sessioninfos;
    }

    public function removeSessioninfos(Sessioninfos $sessioninfos)
    {
        $this->sessioninfos->removeElement($sessioninfos);
    }

    public function getSessioninfos()
    {
        return $this->sessioninfos;
    }
}
