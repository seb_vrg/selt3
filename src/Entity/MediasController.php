<?php

namespace App\Controller;

use App\Entity\Documentlinked;
use App\Entity\Mediastock;
use App\Entity\Documentpdf;
use App\Form\DocumentLinkedType;
use App\Pdf\InfosPdfImplementation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Category;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Validator\Validation;

class MediasController extends AbstractController
{
    /**
     * @Route("/agency/medias", name="medias_home")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function index()
    {
        return $this->render('medias/index.html.twig', [
            'controller_name' => 'MediasController',
        ]);
    }

    /**
     * @Route("/agency/choiceMedia",name="choice_media")
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function choiceMedia(Request $request, ObjectManager $manager)
    {

        if ($request->isMethod('POST'))
        {
            $typeMedia = $request->request->get('typeM');

           if ($typeMedia == 'pdf')
           {
               return $this->redirectToRoute('add_pdf',[]);
           } else {
               return $this->redirectToRoute('add_doc_linked');
           }

        }


        $choice=FALSE;
        return $this->render('medias/add_media.html.twig',[
          //  'type'=>$type_media,
            'request'=>$request,
            'choice'=>$choice,

        ]);
    }

    /**
     * @Route("/agency/addLink/{id}",defaults={"id":null},name="add_doc_linked")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addDocLinked(Request $request,EntityManagerInterface $manager,$id)
    {

        if (is_null($id)) { // CREATION
            $docLinked = new Documentlinked();
            $creation = true;
        }else {  // MODIFICATION
            $docLinked = $manager->find(DocumentLinked::class,$id);
            $nameToUpdate = $docLinked->getName();
            $creation = false;


        }

        $form = $this->createForm(DocumentLinkedType::class,$docLinked);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $typeLink = $docLinked->getType();
            $nameLink = $docLinked->getName();

            if ($typeLink == 'videolink')
            {
                $linkYt =$docLinked->getLink();
                $link = str_ireplace('watch?v=','embed/',$linkYt);


            } else {
                //PDF LINK
                $link = $docLinked->getLink();
            }

            $docLinked->setType($typeLink);
            $docLinked->setLink($link);
            $docLinked->setName($nameLink);
            $manager->persist($docLinked);
            $manager->flush();
            if ($creation == true) {
                $this->addFlash('success', "Le lien <b>$nameLink</b> a bien été enregistré.");

            } else {
                $this->addFlash('info',"<b>LE LIEN $nameToUpdate A BIEN ETE MODIFIE</b>");
            }
            return $this->redirectToRoute('listmedias');
        }
        return $this->render('medias/add_doc_linked.html.twig',[
            'form' => $form->createView(),
            'creation' => $creation
        ]);
    }

    /**
     * @Route("/agency/deleteLink/{id}",name="delete_link")
     * @param Documentlinked $link
     * @param EntityManagerInterface $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteLink(Documentlinked $link,EntityManagerInterface $manager)
    {
        if (is_null($link)) {
            $this->addFlash('warning',"<b>DOCUMENT INCONNU !</b>");
        } else {
            $linkName = $link->getName();
            $manager->remove($link);
            $manager->flush();

            $this->addFlash('success', "Le lien <b>$linkName</b> a bien été supprimé.");
        }
        return $this->redirectToRoute('listmedias');
    }

    /**
     * @Route("/agency/addPdf", name="add_pdf")
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function addPdf(Request $request,ObjectManager $manager)
    {

            $pdf = new Documentpdf();




        $formUpload = $this->createFormBuilder($pdf)

            ->add('documentFile',FileType::class,[
                'required'=>TRUE,
                'label'=>'PDF',
                'multiple'=>FALSE,
                'attr'=>array(
                    'class'=>'custom-file-input'
                )
            ])
            ->add('submit',SubmitType::class,[
                'label'=>'Ajouter'
            ])
            ->getForm()
        ;
        $formUpload->handleRequest($request);
        $viewForm = $formUpload->createView();

        if ($formUpload->isSubmitted() && $formUpload->isValid())
        {
           // $data = $request->request->all();
            $dataFile = $formUpload['documentFile']->getData();
            $pdfName = $dataFile->getClientOriginalName();
            $fileSize = $dataFile->getSize();

            $validator = Validation::createValidator();

            $errors = $validator->validate($pdf);


            if (0 !== count($errors)) {
                return $this->render('medias/errorUploadPdf.html.twig',[
                    'errors'=>$errors]);
            }
            else {
                $destination = 'upload/pdf';
                try{
                    $dataFile->move($destination,$pdfName);
                } catch (FileException $e)
                {
                    $errors = $e;
                    return $this->render('medias/errorUploadPdf.html.twig',[
                        'errors'=>$errors
                    ]);
                }

                $pdf->setDocumentFile($dataFile);
                $pdf->setName($pdfName);
                $pdf->setDocumentSize($fileSize);
                $manager->persist($pdf);
                $manager->flush();
                if (null != $pdf->getId()) {

                    $pdfRef = $pdf->getId();
                    return $this->redirectToRoute('resultUpload',['id'=>$pdfRef]);
                }
            }

        }
        return $this->render('medias/add_pdf.html.twig',[
            'formUpload' => $viewForm,

        ]);
    }

    /**
     * @Route("/agency/deletePdf/{pdf}", name="delete_pdf_file")
     * @param Documentpdf $pdf
     * @param EntityManagerInterface $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deletePdfFile(Documentpdf $pdf,EntityManagerInterface $manager)
    {


        $path = 'upload/pdf/';
        $fileName = $pdf->getName();
        $pathFile = $path.$pdf->getName();
        $confDelete = unlink($pathFile);


        if ($confDelete) {
            $manager->remove($pdf);
            $manager->flush();

            $this->addFlash('success',"Le fichier <b>$fileName</b> a bien été supprimé.");
        } else {
            $this->addFlash('error',"Un problème est survenu lors de la suppression du fichier.");
        }



        return $this->redirectToRoute('listmedias');
    }

    /**
     * @Route("/agency/result/{id}",name="resultUpload")
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param Documentpdf $pdf
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resultUpload(Documentpdf $pdf)
    {

        $confirmationUploadService = new InfosPdfImplementation();
        $message = $confirmationUploadService->getInfosUploadPdf($pdf);

        $pdfId = $pdf->getId();
        return $this->render('medias/confirmationUploadPdf.html.twig',[
           'message'=>$message,
            'id'=>$pdfId
        ]);
    }

    /**
     * @Route("/agency/viewPdf/{pdf}",name="view_pdf")
     * @param Documentpdf $pdf
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewPdf(Documentpdf $pdf)
    {
        $pdfName =$pdf->getName();
        $path = 'upload/pdf/';
        $fileName = $path.$pdfName;

        $confirmationUploadService = new InfosPdfImplementation();
        $description = $confirmationUploadService->getInfosUploadPdf($pdf);

        return $this->render('medias/view_pdf.html.twig',[
            'file'=>$fileName,
            'description'=>$description
        ]);
    }

    /**
     * @Route("/agency/viewLink/{id}",name="view_link")
     * @param Documentlinked $youTubeLink
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewLinkYouTube(Documentlinked $youTubeLink)
    {
        return $this->render('medias/view_youtube_link.html.twig',[
            'link' => $youTubeLink
        ]);
    }

    /**
     * @Route("/agency/listMedias",name="listmedias")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listMedias()
    {

        //LIST LINKS//
        $repo_doc_linked = $this->getDoctrine()->getRepository(Documentlinked::class);
        $listDocLinked = $repo_doc_linked->findAll();
        $nbDocLinked = count($listDocLinked);


        //LIST PDF //
        $repo_pdf = $this->getDoctrine()->getRepository(Documentpdf::class);
        $listPdf = $repo_pdf->findAll();
        $nbPdf = count($listPdf);

        return $this->render('medias/list_medias.html.twig',[

            'list_pdf'=>$listPdf,
            'nbPdf'=>$nbPdf,
            'list_links' => $listDocLinked,
            'nbLinks' => $nbDocLinked
        ]);

    }
}
