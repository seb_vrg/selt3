<?php

namespace App\Entity;


use App\Entity\Documentpdf;
use App\Entity\Studentgroup;
use App\Entity\Questionnaire;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Table(name="mediaslist")
 * @ORM\Entity(repositoryClass="App\Repository\MediaslistRepository")
 */
class Mediaslist
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="name",type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var Studentgroup
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Studentgroup",inversedBy="mediaslist")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="studentgroup_id",referencedColumnName="id")
     *     })
     */
    private $studentgroup;

    /**
     * @var Questionnaire
     * 
     * @ORM\ManyToMany(targetEntity="App\Entity\Questionnaire")
     * @JoinTable(name="list_questionnaire",
     *      joinColumns={@JoinColumn(name="mediaslist_id",referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="questionnaire_id",referencedColumnName="id")}
     * )
     */
    private $questionnaire;



    /**
     * @var Documentpdf
     * @ORM\ManyToMany(targetEntity="App\Entity\Documentpdf",cascade={"persist","remove" })
     * @JoinTable(name="list_pdf",
     *      joinColumns={@JoinColumn(name="madialist_id",referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="pdf_list",referencedColumnName="id")}
     * )
     * 
     */
    private $documentpdf;



    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Documentlinked", cascade={"persist","remove" })
     * @JoinTable(name="list_links",
     *     joinColumns={@JoinColumn(name="medialist_id",referencedColumnName="id")},
     *     inverseJoinColumns={@JoinColumn(name="links_list",referencedColumnName="id")}
     *
     * )
     *
     */
    private $documentlinked;


    public function __construct()
    {
        $this->questionnaire = new ArrayCollection();
        $this->documentpdf = new ArrayCollection();
        $this->documentlinked = new ArrayCollection();
    }


    /**
     * @param Documentpdf $documentpdf
     * @return $this
     */
    public function addDocumentPdf(Documentpdf $documentpdf)
    {

        $this->documentpdf[] = $documentpdf;
        return $this;

    }
    /**
     * Remove documentpdf
     * 
     * @param Documentpdf $documentpdf
     */
    public function removeDocumentPdf(Documentpdf $documentpdf)
    {
        $this->documentpdf->removeElement($documentpdf);
    }

    /**
     * Get documentpdf
     * 
     * 
     */
    public function getDocumentPdf()
    {
        return $this->documentpdf;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStudentgroup(): ?Studentgroup
    {
        return $this->studentgroup;
    }

    public function setStudentgroup(?Studentgroup $studentgroup): self
    {
        $this->studentgroup = $studentgroup;

        return $this;
    }

    public function addQuestionnaire(Questionnaire $questionnaire)
    {
        $this->questionnaire[] = $questionnaire;
        return $this;
    } 

    public function removeQuestionnaire(Questionnaire $questionnaire)
    {
        $this->questionnaire->removeElement($questionnaire);
    }

    public function getQuestionnaire()
    {
        return $this->questionnaire;
    }




    /**
     * @return Collection|Documentlinked[]
     */
    public function addDocumentlinked(Documentlinked $documentlinked): self
    {
        if (!$this->documentlinkeds->contains($documentlinked)) {
            $this->documentlinkeds[] = $documentlinked;
            $documentlinked->addMediaslist($this);
        }

        return $this;
    }

    public function removeDocumentlinked(Documentlinked $documentlinked): self
    {
        if ($this->documentlinkeds->contains($documentlinked)) {
            $this->documentlinkeds->removeElement($documentlinked);
            $documentlinked->removeMediaslist($this);
        }

        return $this;
    }

    /**
     * @return Collection|Documentlinked[]
     */
    public function getDocumentlinked(): Collection
    {
        return $this->documentlinked;
    }
}
