<?php

namespace App\Entity;

use App\Entity\Repax;
use App\Entity\Mediaslist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="questionnaire")
 * @ORM\Entity(repositoryClass="App\Repository\QuestionnaireRepository")
 */
class Questionnaire
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="name",type="string",length=255,nullable=false)
     */
    private $name;

    /**
     * @var Repax
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\Repax",mappedBy="questionnaire")
     */
    private $repax;

    /**
     * @var Mediaslist
     * 
     * @ORM\ManyToMany(targetEntity="App\Entity\Mediaslist")
     * @JoinTable(name="questionnaire_list",
     *      joinColumns={@JoinColumn(name="questionnaire_id",referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="mediaslist_id",referencedColumnName="id")}
     * )
     */
    private $mediaslist;

    //-----------------------Question 1

   

    /**
     * @var string
     *
     * @ORM\Column(name="question1", type="string", length=300,nullable=true,unique=false)
     * 
     * @Assert\NotBlank(message="La question 1 est obligatoire !")
     * @Assert\Length(
     * min=3,
     * max=300,
     * minMessage="La question doit comprendre au moins 3 caractères !",
     * maxMessage="La question ne doit pas exceder 300 caractères !"
     * )
     */
    private $question1;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="rep_question1",type="string",length=300,nullable=false,unique=false)
     * 
     * @Assert\NotBlank(message="La question 1 est obligatoire !")
     */
    private $rep_question1;
    /**
     * @var string
     * 
     * @ORM\Column(name="choix1Q1",type="string",length=100,nullable=false,unique=false)
     * 
     */
    private $choix1Q1;
    /**
     * @var string
     * 
     * @ORM\Column(name="choix2Q1",type="string",length=100,nullable=false,unique=false)
     */
    private $choix2q1;
 //--------------------Question 2   
  
    /**
     * @var string
     * 
     * @ORM\Column(name="question2",type="string",length=300,nullable=false,unique=false)
     * 
     * 
     */
    private $question2;
    /**
     * @var string
     * 
     * @ORM\Column(name="rep_question2",type="string",length=300,nullable=true,unique=false)
     */
    private  $rep_question2;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q2",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q2;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q2",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q2;
//---------------------Question 3
  
    /**
     * @var string
     *
     * @ORM\Column(name="question3",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question3;
    /**
     * @var string
     * 
     * @ORM\Column(name="rep_question3",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question3;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q3",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q3;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q3",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q3;
//-----------------------Question 4
  
    /**
     * @var string
     *
     * @ORM\Column(name="question4",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question4;
    /**
     * @var string
     * 
     * @ORM\Column(name="rep_question4",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question4;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q4",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q4;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q4",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q4;
//-------------------------Question 5
   
    /**
     * @var string
     *
     * @ORM\Column(name="question5",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question5;
    /**
     * @var string
     * 
     * @ORM\Column(name="rep_question5",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question5;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q5",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q5;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q5",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q5;
//---------------------------Question 6
  
    /**
     * @var string
     *
     * @ORM\Column(name="question6",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question6;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question6",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question6;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q6",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q6;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q6",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q6;
    
//----------------------------Question 7
   
    /**
     * @var string
     *
     * @ORM\Column(name="question7",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question7;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question7",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question7;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q7",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q7;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q7",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q7;
//------------------------------Question 8
  
    /**
     * @var string
     *
     * @ORM\Column(name="question8",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question8;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question8",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question8;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q8",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q8;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q8",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q8;
//-------------------------------Question 9
   
    /**
     * @var string
     *
     * @ORM\Column(name="question9",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question9;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question9",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question9;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q9",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q9;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q9",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q9;
//-------------------------------Question 10
  
    /**
     * @var string
     *
     * @ORM\Column(name="question10",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question10;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question10",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question10;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q10",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q10;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q10",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q10;
//-----------------------------Question 11
   
    /**
     * @var string
     *
     * @ORM\Column(name="question11",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question11;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question11",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question11;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q11",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q11;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q11",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q11;
//---------------------------Question 12
   
    /**
     * @var string
     *
     * @ORM\Column(name="question12",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question12;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question12",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question12;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q12",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q12;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q12",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q12;
//----------------------------Question 13
   
    /**
     * @var string
     *
     * @ORM\Column(name="question13",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question13;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question13",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question13;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q13",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q13;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q13",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q13;
//-----------------------------Question 14
   
    /**
     * @var string
     *
     * @ORM\Column(name="question14",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question14;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question14",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question14;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q14",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q14;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q14",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q14;
//---------------------------Question 15
   
    /**
     * @var string
     *
     * @ORM\Column(name="question15",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question15;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question15",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question15;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q15",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q15;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q15",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q15;
//------------------------- Question 16
  
    /**
     * @var string
     *
     * @ORM\Column(name="question16",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question16;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question16",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question16;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q16",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q16;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q16",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q16;
//---------------------------Question 17
   
    /**
     * @var string
     *
     * @ORM\Column(name="question17",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question17;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question17",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question17;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q17",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q17;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q17",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q17;
//--------------------------Question 18
   
    /**
     * @var string
     *
     * @ORM\Column(name="question18",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question18;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question18",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question18;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q18",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q18;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q18",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q18;
//----------------------------Question 19
   
    /**
     * @var string
     *
     * @ORM\Column(name="question19",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question19;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question19",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question19;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q19",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q19;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q19",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q19;
//-------------------------Question 20
  
    /**
     * @var string
     *
     * @ORM\Column(name="question20",type="string",length=300,nullable=true,unique=false)
     *
     */
    private $question20;
    /**
     * @var string
     *
     * @ORM\Column(name="rep_question20",type="string",length=300,nullable=true,unique=false)
     */
    private $rep_question20;
    /**
     * @var string
     *
     * @ORM\Column(name="choix1Q20",type="string",length=100,nullable=true,unique=false)
     *
     */
    private $choix1Q20;
    /**
     * @var string
     *
     * @ORM\Column(name="choix2Q20",type="string",length=100,nullable=true,unique=false)
     */
    private $choix2Q20;

    public function __toString()
    {
        return $this->name;
    }

    public function __construct()
    {
        $this->repax = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
     /**
     * Set question1
     *
     * @param string $question1
     *
     * @return questionnaire
     */
    public function setQuestion1($question1)
    {
        $this->question1 = $question1;

        return $this;
    }

    /**
     * Get question1
     *
     * @return string
     */
    public function getQuestion1()
    {
        return $this->question1;
    }

    /**
     * Set repQuestion1
     *
     * @param string $repQuestion1
     *
     * @return questionnaire
     */
    public function setRepQuestion1($repQuestion1)
    {
        $this->rep_question1 = $repQuestion1;

        return $this;
    }

    /**
     * Get repQuestion1
     *
     * @return string
     */
    public function getRepQuestion1()
    {
        return $this->rep_question1;
    }

    /**
     * Set choix1Q1
     *
     * @param string $choix1Q1
     *
     * @return questionnaire
     */
    public function setChoix1Q1($choix1Q1)
    {
        $this->choix1Q1 = $choix1Q1;

        return $this;
    }

    /**
     * Get choix1Q1
     *
     * @return string
     */
    public function getChoix1Q1()
    {
        return $this->choix1Q1;
    }

    /**
     * Set choix2q1
     *
     * @param string $choix2q1
     *
     * @return questionnaire
     */
    public function setChoix2q1($choix2q1)
    {
        $this->choix2q1 = $choix2q1;

        return $this;
    }

    /**
     * Get choix2q1
     *
     * @return string
     */
    public function getChoix2q1()
    {
        return $this->choix2q1;
    }

    /**
     * Set question2
     *
     * @param string $question2
     *
     * @return questionnaire
     */
    public function setQuestion2($question2)
    {
        $this->question2 = $question2;

        return $this;
    }

    /**
     * Get question2
     *
     * @return string
     */
    public function getQuestion2()
    {
        return $this->question2;
    }

    /**
     * Set repQuestion2
     *
     * @param string $repQuestion2
     *
     * @return questionnaire
     */
    public function setRepQuestion2($repQuestion2)
    {
        $this->rep_question2 = $repQuestion2;

        return $this;
    }

    /**
     * Get repQuestion2
     *
     * @return string
     */
    public function getRepQuestion2()
    {
        return $this->rep_question2;
    }

    /**
     * Set choix1Q2
     *
     * @param string $choix1Q2
     *
     * @return questionnaire
     */
    public function setChoix1Q2($choix1Q2)
    {
        $this->choix1Q2 = $choix1Q2;

        return $this;
    }

    /**
     * Get choix1Q2
     *
     * @return string
     */
    public function getChoix1Q2()
    {
        return $this->choix1Q2;
    }

    /**
     * Set choix2Q2
     *
     * @param string $choix2Q2
     *
     * @return questionnaire
     */
    public function setChoix2Q2($choix2Q2)
    {
        $this->choix2Q2 = $choix2Q2;

        return $this;
    }

    /**
     * Get choix2Q2
     *
     * @return string
     */
    public function getChoix2Q2()
    {
        return $this->choix2Q2;
    }

    /**
     * Set question3
     *
     * @param string $question3
     *
     * @return questionnaire
     */
    public function setQuestion3($question3)
    {
        $this->question3 = $question3;

        return $this;
    }

    /**
     * Get question3
     *
     * @return string
     */
    public function getQuestion3()
    {
        return $this->question3;
    }

    /**
     * Set repQuestion3
     *
     * @param string $repQuestion3
     *
     * @return questionnaire
     */
    public function setRepQuestion3($repQuestion3)
    {
        $this->rep_question3 = $repQuestion3;

        return $this;
    }

    /**
     * Get repQuestion3
     *
     * @return string
     */
    public function getRepQuestion3()
    {
        return $this->rep_question3;
    }

    /**
     * Set choix1Q3
     *
     * @param string $choix1Q3
     *
     * @return questionnaire
     */
    public function setChoix1Q3($choix1Q3)
    {
        $this->choix1Q3 = $choix1Q3;

        return $this;
    }

    /**
     * Get choix1Q3
     *
     * @return string
     */
    public function getChoix1Q3()
    {
        return $this->choix1Q3;
    }

    /**
     * Set choix2Q3
     *
     * @param string $choix2Q3
     *
     * @return questionnaire
     */
    public function setChoix2Q3($choix2Q3)
    {
        $this->choix2Q3 = $choix2Q3;

        return $this;
    }

    /**
     * Get choix2Q3
     *
     * @return string
     */
    public function getChoix2Q3()
    {
        return $this->choix2Q3;
    }

    /**
     * Set question4
     *
     * @param string $question4
     *
     * @return questionnaire
     */
    public function setQuestion4($question4)
    {
        $this->question4 = $question4;

        return $this;
    }

    /**
     * Get question4
     *
     * @return string
     */
    public function getQuestion4()
    {
        return $this->question4;
    }

    /**
     * Set repQuestion4
     *
     * @param string $repQuestion4
     *
     * @return questionnaire
     */
    public function setRepQuestion4($repQuestion4)
    {
        $this->rep_question4 = $repQuestion4;

        return $this;
    }

    /**
     * Get repQuestion4
     *
     * @return string
     */
    public function getRepQuestion4()
    {
        return $this->rep_question4;
    }

    /**
     * Set choix1Q4
     *
     * @param string $choix1Q4
     *
     * @return questionnaire
     */
    public function setChoix1Q4($choix1Q4)
    {
        $this->choix1Q4 = $choix1Q4;

        return $this;
    }

    /**
     * Get choix1Q4
     *
     * @return string
     */
    public function getChoix1Q4()
    {
        return $this->choix1Q4;
    }

    /**
     * Set choix2Q4
     *
     * @param string $choix2Q4
     *
     * @return questionnaire
     */
    public function setChoix2Q4($choix2Q4)
    {
        $this->choix2Q4 = $choix2Q4;

        return $this;
    }

    /**
     * Get choix2Q4
     *
     * @return string
     */
    public function getChoix2Q4()
    {
        return $this->choix2Q4;
    }

    /**
     * Set question5
     *
     * @param string $question5
     *
     * @return questionnaire
     */
    public function setQuestion5($question5)
    {
        $this->question5 = $question5;

        return $this;
    }

    /**
     * Get question5
     *
     * @return string
     */
    public function getQuestion5()
    {
        return $this->question5;
    }

    /**
     * Set repQuestion5
     *
     * @param string $repQuestion5
     *
     * @return questionnaire
     */
    public function setRepQuestion5($repQuestion5)
    {
        $this->rep_question5 = $repQuestion5;

        return $this;
    }

    /**
     * Get repQuestion5
     *
     * @return string
     */
    public function getRepQuestion5()
    {
        return $this->rep_question5;
    }

    /**
     * Set choix1Q5
     *
     * @param string $choix1Q5
     *
     * @return questionnaire
     */
    public function setChoix1Q5($choix1Q5)
    {
        $this->choix1Q5 = $choix1Q5;

        return $this;
    }

    /**
     * Get choix1Q5
     *
     * @return string
     */
    public function getChoix1Q5()
    {
        return $this->choix1Q5;
    }

    /**
     * Set choix2Q5
     *
     * @param string $choix2Q5
     *
     * @return questionnaire
     */
    public function setChoix2Q5($choix2Q5)
    {
        $this->choix2Q5 = $choix2Q5;

        return $this;
    }

    /**
     * Get choix2Q5
     *
     * @return string
     */
    public function getChoix2Q5()
    {
        return $this->choix2Q5;
    }

    /**
     * Set question6
     *
     * @param string $question6
     *
     * @return questionnaire
     */
    public function setQuestion6($question6)
    {
        $this->question6 = $question6;

        return $this;
    }

    /**
     * Get question6
     *
     * @return string
     */
    public function getQuestion6()
    {
        return $this->question6;
    }

    /**
     * Set repQuestion6
     *
     * @param string $repQuestion6
     *
     * @return questionnaire
     */
    public function setRepQuestion6($repQuestion6)
    {
        $this->rep_question6 = $repQuestion6;

        return $this;
    }

    /**
     * Get repQuestion6
     *
     * @return string
     */
    public function getRepQuestion6()
    {
        return $this->rep_question6;
    }

    /**
     * Set choix1Q6
     *
     * @param string $choix1Q6
     *
     * @return questionnaire
     */
    public function setChoix1Q6($choix1Q6)
    {
        $this->choix1Q6 = $choix1Q6;

        return $this;
    }

    /**
     * Get choix1Q6
     *
     * @return string
     */
    public function getChoix1Q6()
    {
        return $this->choix1Q6;
    }

    /**
     * Set choix2Q6
     *
     * @param string $choix2Q6
     *
     * @return questionnaire
     */
    public function setChoix2Q6($choix2Q6)
    {
        $this->choix2Q6 = $choix2Q6;

        return $this;
    }

    /**
     * Get choix2Q6
     *
     * @return string
     */
    public function getChoix2Q6()
    {
        return $this->choix2Q6;
    }

    /**
     * Set question7
     *
     * @param string $question7
     *
     * @return questionnaire
     */
    public function setQuestion7($question7)
    {
        $this->question7 = $question7;

        return $this;
    }

    /**
     * Get question7
     *
     * @return string
     */
    public function getQuestion7()
    {
        return $this->question7;
    }

    /**
     * Set repQuestion7
     *
     * @param string $repQuestion7
     *
     * @return questionnaire
     */
    public function setRepQuestion7($repQuestion7)
    {
        $this->rep_question7 = $repQuestion7;

        return $this;
    }

    /**
     * Get repQuestion7
     *
     * @return string
     */
    public function getRepQuestion7()
    {
        return $this->rep_question7;
    }

    /**
     * Set choix1Q7
     *
     * @param string $choix1Q7
     *
     * @return questionnaire
     */
    public function setChoix1Q7($choix1Q7)
    {
        $this->choix1Q7 = $choix1Q7;

        return $this;
    }

    /**
     * Get choix1Q7
     *
     * @return string
     */
    public function getChoix1Q7()
    {
        return $this->choix1Q7;
    }

    /**
     * Set choix2Q7
     *
     * @param string $choix2Q7
     *
     * @return questionnaire
     */
    public function setChoix2Q7($choix2Q7)
    {
        $this->choix2Q7 = $choix2Q7;

        return $this;
    }

    /**
     * Get choix2Q7
     *
     * @return string
     */
    public function getChoix2Q7()
    {
        return $this->choix2Q7;
    }

    /**
     * Set question8
     *
     * @param string $question8
     *
     * @return questionnaire
     */
    public function setQuestion8($question8)
    {
        $this->question8 = $question8;

        return $this;
    }

    /**
     * Get question8
     *
     * @return string
     */
    public function getQuestion8()
    {
        return $this->question8;
    }

    /**
     * Set repQuestion8
     *
     * @param string $repQuestion8
     *
     * @return questionnaire
     */
    public function setRepQuestion8($repQuestion8)
    {
        $this->rep_question8 = $repQuestion8;

        return $this;
    }

    /**
     * Get repQuestion8
     *
     * @return string
     */
    public function getRepQuestion8()
    {
        return $this->rep_question8;
    }

    /**
     * Set choix1Q8
     *
     * @param string $choix1Q8
     *
     * @return questionnaire
     */
    public function setChoix1Q8($choix1Q8)
    {
        $this->choix1Q8 = $choix1Q8;

        return $this;
    }

    /**
     * Get choix1Q8
     *
     * @return string
     */
    public function getChoix1Q8()
    {
        return $this->choix1Q8;
    }

    /**
     * Set choix2Q8
     *
     * @param string $choix2Q8
     *
     * @return questionnaire
     */
    public function setChoix2Q8($choix2Q8)
    {
        $this->choix2Q8 = $choix2Q8;

        return $this;
    }

    /**
     * Get choix2Q8
     *
     * @return string
     */
    public function getChoix2Q8()
    {
        return $this->choix2Q8;
    }

    /**
     * Set question9
     *
     * @param string $question9
     *
     * @return questionnaire
     */
    public function setQuestion9($question9)
    {
        $this->question9 = $question9;

        return $this;
    }

    /**
     * Get question9
     *
     * @return string
     */
    public function getQuestion9()
    {
        return $this->question9;
    }

    /**
     * Set repQuestion9
     *
     * @param string $repQuestion9
     *
     * @return questionnaire
     */
    public function setRepQuestion9($repQuestion9)
    {
        $this->rep_question9 = $repQuestion9;

        return $this;
    }

    /**
     * Get repQuestion9
     *
     * @return string
     */
    public function getRepQuestion9()
    {
        return $this->rep_question9;
    }

    /**
     * Set choix1Q9
     *
     * @param string $choix1Q9
     *
     * @return questionnaire
     */
    public function setChoix1Q9($choix1Q9)
    {
        $this->choix1Q9 = $choix1Q9;

        return $this;
    }

    /**
     * Get choix1Q9
     *
     * @return string
     */
    public function getChoix1Q9()
    {
        return $this->choix1Q9;
    }

    /**
     * Set choix2Q9
     *
     * @param string $choix2Q9
     *
     * @return questionnaire
     */
    public function setChoix2Q9($choix2Q9)
    {
        $this->choix2Q9 = $choix2Q9;

        return $this;
    }

    /**
     * Get choix2Q9
     *
     * @return string
     */
    public function getChoix2Q9()
    {
        return $this->choix2Q9;
    }

    /**
     * Set question10
     *
     * @param string $question10
     *
     * @return questionnaire
     */
    public function setQuestion10($question10)
    {
        $this->question10 = $question10;

        return $this;
    }

    /**
     * Get question10
     *
     * @return string
     */
    public function getQuestion10()
    {
        return $this->question10;
    }

    /**
     * Set repQuestion10
     *
     * @param string $repQuestion10
     *
     * @return questionnaire
     */
    public function setRepQuestion10($repQuestion10)
    {
        $this->rep_question10 = $repQuestion10;

        return $this;
    }

    /**
     * Get repQuestion10
     *
     * @return string
     */
    public function getRepQuestion10()
    {
        return $this->rep_question10;
    }

    /**
     * Set choix1Q10
     *
     * @param string $choix1Q10
     *
     * @return questionnaire
     */
    public function setChoix1Q10($choix1Q10)
    {
        $this->choix1Q10 = $choix1Q10;

        return $this;
    }

    /**
     * Get choix1Q10
     *
     * @return string
     */
    public function getChoix1Q10()
    {
        return $this->choix1Q10;
    }

    /**
     * Set choix2Q10
     *
     * @param string $choix2Q10
     *
     * @return questionnaire
     */
    public function setChoix2Q10($choix2Q10)
    {
        $this->choix2Q10 = $choix2Q10;

        return $this;
    }

    /**
     * Get choix2Q10
     *
     * @return string
     */
    public function getChoix2Q10()
    {
        return $this->choix2Q10;
    }

    /**
     * Set question11
     *
     * @param string $question11
     *
     * @return questionnaire
     */
    public function setQuestion11($question11)
    {
        $this->question11 = $question11;

        return $this;
    }

    /**
     * Get question11
     *
     * @return string
     */
    public function getQuestion11()
    {
        return $this->question11;
    }

    /**
     * Set repQuestion11
     *
     * @param string $repQuestion11
     *
     * @return questionnaire
     */
    public function setRepQuestion11($repQuestion11)
    {
        $this->rep_question11 = $repQuestion11;

        return $this;
    }

    /**
     * Get repQuestion11
     *
     * @return string
     */
    public function getRepQuestion11()
    {
        return $this->rep_question11;
    }

    /**
     * Set choix1Q11
     *
     * @param string $choix1Q11
     *
     * @return questionnaire
     */
    public function setChoix1Q11($choix1Q11)
    {
        $this->choix1Q11 = $choix1Q11;

        return $this;
    }

    /**
     * Get choix1Q11
     *
     * @return string
     */
    public function getChoix1Q11()
    {
        return $this->choix1Q11;
    }

    /**
     * Set choix2Q11
     *
     * @param string $choix2Q11
     *
     * @return questionnaire
     */
    public function setChoix2Q11($choix2Q11)
    {
        $this->choix2Q11 = $choix2Q11;

        return $this;
    }

    /**
     * Get choix2Q11
     *
     * @return string
     */
    public function getChoix2Q11()
    {
        return $this->choix2Q11;
    }

    /**
     * Set question12
     *
     * @param string $question12
     *
     * @return questionnaire
     */
    public function setQuestion12($question12)
    {
        $this->question12 = $question12;

        return $this;
    }

    /**
     * Get question12
     *
     * @return string
     */
    public function getQuestion12()
    {
        return $this->question12;
    }

    /**
     * Set repQuestion12
     *
     * @param string $repQuestion12
     *
     * @return questionnaire
     */
    public function setRepQuestion12($repQuestion12)
    {
        $this->rep_question12 = $repQuestion12;

        return $this;
    }

    /**
     * Get repQuestion12
     *
     * @return string
     */
    public function getRepQuestion12()
    {
        return $this->rep_question12;
    }

    /**
     * Set choix1Q12
     *
     * @param string $choix1Q12
     *
     * @return questionnaire
     */
    public function setChoix1Q12($choix1Q12)
    {
        $this->choix1Q12 = $choix1Q12;

        return $this;
    }

    /**
     * Get choix1Q12
     *
     * @return string
     */
    public function getChoix1Q12()
    {
        return $this->choix1Q12;
    }

    /**
     * Set choix2Q12
     *
     * @param string $choix2Q12
     *
     * @return questionnaire
     */
    public function setChoix2Q12($choix2Q12)
    {
        $this->choix2Q12 = $choix2Q12;

        return $this;
    }

    /**
     * Get choix2Q12
     *
     * @return string
     */
    public function getChoix2Q12()
    {
        return $this->choix2Q12;
    }

    /**
     * Set question13
     *
     * @param string $question13
     *
     * @return questionnaire
     */
    public function setQuestion13($question13)
    {
        $this->question13 = $question13;

        return $this;
    }

    /**
     * Get question13
     *
     * @return string
     */
    public function getQuestion13()
    {
        return $this->question13;
    }

    /**
     * Set repQuestion13
     *
     * @param string $repQuestion13
     *
     * @return questionnaire
     */
    public function setRepQuestion13($repQuestion13)
    {
        $this->rep_question13 = $repQuestion13;

        return $this;
    }

    /**
     * Get repQuestion13
     *
     * @return string
     */
    public function getRepQuestion13()
    {
        return $this->rep_question13;
    }

    /**
     * Set choix1Q13
     *
     * @param string $choix1Q13
     *
     * @return questionnaire
     */
    public function setChoix1Q13($choix1Q13)
    {
        $this->choix1Q13 = $choix1Q13;

        return $this;
    }

    /**
     * Get choix1Q13
     *
     * @return string
     */
    public function getChoix1Q13()
    {
        return $this->choix1Q13;
    }

    /**
     * Set choix2Q13
     *
     * @param string $choix2Q13
     *
     * @return questionnaire
     */
    public function setChoix2Q13($choix2Q13)
    {
        $this->choix2Q13 = $choix2Q13;

        return $this;
    }

    /**
     * Get choix2Q13
     *
     * @return string
     */
    public function getChoix2Q13()
    {
        return $this->choix2Q13;
    }

    /**
     * Set question14
     *
     * @param string $question14
     *
     * @return questionnaire
     */
    public function setQuestion14($question14)
    {
        $this->question14 = $question14;

        return $this;
    }

    /**
     * Get question14
     *
     * @return string
     */
    public function getQuestion14()
    {
        return $this->question14;
    }

    /**
     * Set repQuestion14
     *
     * @param string $repQuestion14
     *
     * @return questionnaire
     */
    public function setRepQuestion14($repQuestion14)
    {
        $this->rep_question14 = $repQuestion14;

        return $this;
    }

    /**
     * Get repQuestion14
     *
     * @return string
     */
    public function getRepQuestion14()
    {
        return $this->rep_question14;
    }

    /**
     * Set choix1Q14
     *
     * @param string $choix1Q14
     *
     * @return questionnaire
     */
    public function setChoix1Q14($choix1Q14)
    {
        $this->choix1Q14 = $choix1Q14;

        return $this;
    }

    /**
     * Get choix1Q14
     *
     * @return string
     */
    public function getChoix1Q14()
    {
        return $this->choix1Q14;
    }

    /**
     * Set choix2Q14
     *
     * @param string $choix2Q14
     *
     * @return questionnaire
     */
    public function setChoix2Q14($choix2Q14)
    {
        $this->choix2Q14 = $choix2Q14;

        return $this;
    }

    /**
     * Get choix2Q14
     *
     * @return string
     */
    public function getChoix2Q14()
    {
        return $this->choix2Q14;
    }

    /**
     * Set question15
     *
     * @param string $question15
     *
     * @return questionnaire
     */
    public function setQuestion15($question15)
    {
        $this->question15 = $question15;

        return $this;
    }

    /**
     * Get question15
     *
     * @return string
     */
    public function getQuestion15()
    {
        return $this->question15;
    }

    /**
     * Set repQuestion15
     *
     * @param string $repQuestion15
     *
     * @return questionnaire
     */
    public function setRepQuestion15($repQuestion15)
    {
        $this->rep_question15 = $repQuestion15;

        return $this;
    }

    /**
     * Get repQuestion15
     *
     * @return string
     */
    public function getRepQuestion15()
    {
        return $this->rep_question15;
    }

    /**
     * Set choix1Q15
     *
     * @param string $choix1Q15
     *
     * @return questionnaire
     */
    public function setChoix1Q15($choix1Q15)
    {
        $this->choix1Q15 = $choix1Q15;

        return $this;
    }

    /**
     * Get choix1Q15
     *
     * @return string
     */
    public function getChoix1Q15()
    {
        return $this->choix1Q15;
    }

    /**
     * Set choix2Q15
     *
     * @param string $choix2Q15
     *
     * @return questionnaire
     */
    public function setChoix2Q15($choix2Q15)
    {
        $this->choix2Q15 = $choix2Q15;

        return $this;
    }

    /**
     * Get choix2Q15
     *
     * @return string
     */
    public function getChoix2Q15()
    {
        return $this->choix2Q15;
    }

    /**
     * Set question16
     *
     * @param string $question16
     *
     * @return questionnaire
     */
    public function setQuestion16($question16)
    {
        $this->question16 = $question16;

        return $this;
    }

    /**
     * Get question16
     *
     * @return string
     */
    public function getQuestion16()
    {
        return $this->question16;
    }

    /**
     * Set repQuestion16
     *
     * @param string $repQuestion16
     *
     * @return questionnaire
     */
    public function setRepQuestion16($repQuestion16)
    {
        $this->rep_question16 = $repQuestion16;

        return $this;
    }

    /**
     * Get repQuestion16
     *
     * @return string
     */
    public function getRepQuestion16()
    {
        return $this->rep_question16;
    }

    /**
     * Set choix1Q16
     *
     * @param string $choix1Q16
     *
     * @return questionnaire
     */
    public function setChoix1Q16($choix1Q16)
    {
        $this->choix1Q16 = $choix1Q16;

        return $this;
    }

    /**
     * Get choix1Q16
     *
     * @return string
     */
    public function getChoix1Q16()
    {
        return $this->choix1Q16;
    }

    /**
     * Set choix2Q16
     *
     * @param string $choix2Q16
     *
     * @return questionnaire
     */
    public function setChoix2Q16($choix2Q16)
    {
        $this->choix2Q16 = $choix2Q16;

        return $this;
    }

    /**
     * Get choix2Q16
     *
     * @return string
     */
    public function getChoix2Q16()
    {
        return $this->choix2Q16;
    }

    /**
     * Set question17
     *
     * @param string $question17
     *
     * @return questionnaire
     */
    public function setQuestion17($question17)
    {
        $this->question17 = $question17;

        return $this;
    }

    /**
     * Get question17
     *
     * @return string
     */
    public function getQuestion17()
    {
        return $this->question17;
    }

    /**
     * Set repQuestion17
     *
     * @param string $repQuestion17
     *
     * @return questionnaire
     */
    public function setRepQuestion17($repQuestion17)
    {
        $this->rep_question17 = $repQuestion17;

        return $this;
    }

    /**
     * Get repQuestion17
     *
     * @return string
     */
    public function getRepQuestion17()
    {
        return $this->rep_question17;
    }

    /**
     * Set choix1Q17
     *
     * @param string $choix1Q17
     *
     * @return questionnaire
     */
    public function setChoix1Q17($choix1Q17)
    {
        $this->choix1Q17 = $choix1Q17;

        return $this;
    }

    /**
     * Get choix1Q17
     *
     * @return string
     */
    public function getChoix1Q17()
    {
        return $this->choix1Q17;
    }

    /**
     * Set choix2Q17
     *
     * @param string $choix2Q17
     *
     * @return questionnaire
     */
    public function setChoix2Q17($choix2Q17)
    {
        $this->choix2Q17 = $choix2Q17;

        return $this;
    }

    /**
     * Get choix2Q17
     *
     * @return string
     */
    public function getChoix2Q17()
    {
        return $this->choix2Q17;
    }

    /**
     * Set question18
     *
     * @param string $question18
     *
     * @return questionnaire
     */
    public function setQuestion18($question18)
    {
        $this->question18 = $question18;

        return $this;
    }

    /**
     * Get question18
     *
     * @return string
     */
    public function getQuestion18()
    {
        return $this->question18;
    }

    /**
     * Set repQuestion18
     *
     * @param string $repQuestion18
     *
     * @return questionnaire
     */
    public function setRepQuestion18($repQuestion18)
    {
        $this->rep_question18 = $repQuestion18;

        return $this;
    }

    /**
     * Get repQuestion18
     *
     * @return string
     */
    public function getRepQuestion18()
    {
        return $this->rep_question18;
    }

    /**
     * Set choix1Q18
     *
     * @param string $choix1Q18
     *
     * @return questionnaire
     */
    public function setChoix1Q18($choix1Q18)
    {
        $this->choix1Q18 = $choix1Q18;

        return $this;
    }

    /**
     * Get choix1Q18
     *
     * @return string
     */
    public function getChoix1Q18()
    {
        return $this->choix1Q18;
    }

    /**
     * Set choix2Q18
     *
     * @param string $choix2Q18
     *
     * @return questionnaire
     */
    public function setChoix2Q18($choix2Q18)
    {
        $this->choix2Q18 = $choix2Q18;

        return $this;
    }

    /**
     * Get choix2Q18
     *
     * @return string
     */
    public function getChoix2Q18()
    {
        return $this->choix2Q18;
    }

    /**
     * Set question19
     *
     * @param string $question19
     *
     * @return questionnaire
     */
    public function setQuestion19($question19)
    {
        $this->question19 = $question19;

        return $this;
    }

    /**
     * Get question19
     *
     * @return string
     */
    public function getQuestion19()
    {
        return $this->question19;
    }

    /**
     * Set repQuestion19
     *
     * @param string $repQuestion19
     *
     * @return questionnaire
     */
    public function setRepQuestion19($repQuestion19)
    {
        $this->rep_question19 = $repQuestion19;

        return $this;
    }

    /**
     * Get repQuestion19
     *
     * @return string
     */
    public function getRepQuestion19()
    {
        return $this->rep_question19;
    }

    /**
     * Set choix1Q19
     *
     * @param string $choix1Q19
     *
     * @return questionnaire
     */
    public function setChoix1Q19($choix1Q19)
    {
        $this->choix1Q19 = $choix1Q19;

        return $this;
    }

    /**
     * Get choix1Q19
     *
     * @return string
     */
    public function getChoix1Q19()
    {
        return $this->choix1Q19;
    }

    /**
     * Set choix2Q19
     *
     * @param string $choix2Q19
     *
     * @return questionnaire
     */
    public function setChoix2Q19($choix2Q19)
    {
        $this->choix2Q19 = $choix2Q19;

        return $this;
    }

    /**
     * Get choix2Q19
     *
     * @return string
     */
    public function getChoix2Q19()
    {
        return $this->choix2Q19;
    }

    /**
     * Set question20
     *
     * @param string $question20
     *
     * @return questionnaire
     */
    public function setQuestion20($question20)
    {
        $this->question20 = $question20;

        return $this;
    }

    /**
     * Get question20
     *
     * @return string
     */
    public function getQuestion20()
    {
        return $this->question20;
    }

    /**
     * Set repQuestion20
     *
     * @param string $repQuestion20
     *
     * @return questionnaire
     */
    public function setRepQuestion20($repQuestion20)
    {
        $this->rep_question20 = $repQuestion20;

        return $this;
    }

    /**
     * Get repQuestion20
     *
     * @return string
     */
    public function getRepQuestion20()
    {
        return $this->rep_question20;
    }

    /**
     * Set choix1Q20
     *
     * @param string $choix1Q20
     *
     * @return questionnaire
     */
    public function setChoix1Q20($choix1Q20)
    {
        $this->choix1Q20 = $choix1Q20;

        return $this;
    }

    /**
     * Get choix1Q20
     *
     * @return string
     */
    public function getChoix1Q20()
    {
        return $this->choix1Q20;
    }

    /**
     * Set choix2Q20
     *
     * @param string $choix2Q20
     *
     * @return questionnaire
     */
    public function setChoix2Q20($choix2Q20)
    {
        $this->choix2Q20 = $choix2Q20;

        return $this;
    }

    /**
     * Get choix2Q20
     *
     * @return string
     */
    public function getChoix2Q20()
    {
        return $this->choix2Q20;
    }


    public function addRepax(Repax $repax)
    {
        $this->repax[] = $repax;
    }

    public function removeRepax(Repax $repax)
    {
        $this->repax->removeElement($repax);
    }

    public function getRepax()
    {
        return $this->repax;
    }

    public function getMediaslist()
    {
        return $this->mediaslist;
    }
}