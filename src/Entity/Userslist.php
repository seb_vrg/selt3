<?php

namespace App\Entity;

use App\Entity\User;
use App\Entity\Sessioninfos;
use App\Entity\Studentgroup;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="userslist")
 * @ORM\Entity(repositoryClass="App\Repository\UserslistRepository")
 */
class Userslist
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="namelist",type="string",length=100,nullable=false,unique=true)
     * @Assert\NotBlank(message="Le nom de la liste est obligatoiore !")
     *
     * @var string
     */
    private $namelist;


    /**
     * @var Company
     * @ORM\ManyToOne(targetEntity="App\Entity\Company",inversedBy="userslist")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="company_id",referencedColumnName="id")
     *     })
     */
    private $company;
    /**
     * @var User
     * 
     * @ORM\ManyToMany(targetEntity="App\Entity\User",cascade={"persist","remove"})
     * @JoinTable(name="stagiaire_in_list",joinColumns={@JoinColumn(name="userslist_id",referencedColumnName="id")},inverseJoinColumns={@JoinColumn(name="user_id",referencedColumnName="id")})
     */
    private $user;

    /**
     * @var Studentgroup
     * 
     * @ORM\ManyToOne(targetEntity="Studentgroup",inversedBy="userslist")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="studentgroup_id",referencedColumnName="id")
     * })
     */
    private $studentgroup;

    /**
     * @var Sessioninfos
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\Sessioninfos",mappedBy="userslist")
     */
    private $sessioninfos;

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->sessioninfos = new ArrayCollection();
    }

    public function __toString()
    {
       return $this->namelist;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNamelist(): ?string
    {
        return $this->namelist;
    }

    /**
     * @return Company|null
     */
    public function getCompany():? Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     * @return Userslist
     */
    public function setCompany(Company $company): Userslist
    {
        $this->company = $company;
        return $this;
    }


    /**
     * @param string $namelist
     * @return Userslist
     */
    public function setNamelist(string $namelist): Userslist
    {
        $this->namelist = $namelist;
        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
        }

        return $this;
    }

    public function getStudentgroup(): ?Studentgroup
    {
        return $this->studentgroup;
    }

    public function setStudentgroup(Studentgroup $studentgroup): self
    {
        $this->studentgroup = $studentgroup;

        return $this;
    }

    public function addSessioninfos(Sessioninfos $sessioninfos)
    {
        $this->sessioninfos[] = $sessioninfos;

        return $this;
    }

    public function removeSessioninfos(Sessionsinfos $sessioninfos)
    {
        $this->sessioninfos->removeElement($sessioninfos);
    }

    public function getSessioninfos()
    {
        return $this->sessioninfos;
    }
}
