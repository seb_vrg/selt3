<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use App\Entity\Mediaslist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation\UploadableField;
use Vich\UploaderBundle\VichUploaderBundle;

/**
 * Document pdf
 * 
 * @ORM\Table(name="documentpdf")
 * @ORM\Entity(repositoryClass="App\Repository\DocumentpdfRepository")
 * @Vich\Uploadable
 */
class Documentpdf
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     */
    private $name;

    /**
     * @Vich\UploadableField(mapping="documentpdf",fileNameProperty="name", size="documentSize")
     * @ORM\Column(type="string",length=255)
     * @Assert\File(
     *      maxSize = "2500k",
     *      maxSizeMessage = "Votre Fichier ne doit pas dépasser 2500 ko !",
     *      mimeTypes={"application/pdf"},
     *      mimeTypesMessage = "Votre fichier n'est pas de type PDF "
     *      
     * )
     * 
     */
    private $documentFile;
    /**
     * @ORM\Column(type="datetime")
     * 
     * @var \DateTimeInterface
     */
    private $updateAt;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     *
     */
    private $documentSize;



    /**
     * @var Mediaslist
     * 
     * @ORM\ManyToMany(targetEntity="App\Entity\Mediaslist")
     * @JoinTable(name="pdf_list",
     *      joinColumns={@JoinColumn(name="pdf_id",referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="medialist_id",referencedColumnName="id")}
     * )
     * 
     */
    private $medialist;

    public function __construct()
    {
        $this->medialist = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @param File|UploadedFile $documentpdf
     * @return $this
     * @throws \Exception
     */
    public function setDocumentFile(File $documentpdf = null)
    {
       $this->documentFile = $documentpdf;
        if ($documentpdf) {
            $this->updateAt = new \DateTime('now');
            return $this;
        }
        
    }
    /**
     * @return VichUploaderBundle|null
     */
    public function getDocumentFile()
    {
        return $this->documentFile;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getDocumentSize(): int
    {
        return $this->documentSize;
    }

    /**
     * @param int $documentSize
     * @return Documentpdf
     */
    public function setDocumentSize(int $documentSize): Documentpdf
    {
        $this->documentSize = $documentSize;
        return $this;
    }


}
