<?php

namespace App\Entity;

use App\Entity\Company as Company;
use App\Entity\Userslist as Userslist;
use App\Entity\Mediaslist as Mediaslist;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * Studentgroup
 *
 * @ORM\Table(name="studentgroup")
 * @ORM\Entity(repositoryClass="App\Repository\StudentgroupRepository")
 */
class Studentgroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="theme", type="string", length=255, nullable=false)
     */
    private $theme;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(name="datedebut", type="datetime", nullable=false)
     */
    private $datedebut;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(name="datefin", type="datetime",  nullable=false)
     */
    private $datefin;

    /**
     * @var int
     *
     * @ORM\Column(name="timestampdebut", type="integer", nullable=false)
     */
    private $timestampdebut;

    /**
     * @var int
     *
     * @ORM\Column(name="timestampfin", type="integer", nullable=false)
     */
    private $timestampfin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isactive", type="boolean", nullable=false)
     */
    private $isactive;

    /**
     * @var Mediaslist
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\Mediaslist",mappedBy="studentgroup",cascade={"persist","remove"})
     */
    private $mediaslist;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Company",inversedBy="studentgroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var Sessioninfos
     * @ORM\OneToMany(targetEntity="App\Entity\Sessioninfos",mappedBy="studentgroup")
     */
    private $sessioninfos;


    /**
     *
     * @var Userslist
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\Userslist",mappedBy="studentgroup",cascade={"persist","remove"})
     */
    private $userslist;


    /**
     * Studentgroup constructor.
     */
    public function __construct()
    {
        $this->mediaslist = new ArrayCollection();
        $this->sessioninfos = new ArrayCollection();
        $this->userslist = new ArrayCollection();
    }

    /**
     * @return Userslist []
     */
    public function getUserslist(): array
    {
        return $this->userslist;
    }

    /**
     * @param Userslist $userslist
     */
    public function setUserslist(Userslist $userslist): void
    {
        $this->userslist = $userslist;
    }

    /**
     * @return Sessioninfos
     */
    public function getSessioninfos(): Sessioninfos
    {
        return $this->sessioninfos;
    }

    /**
     * @param Sessioninfos $sessioninfos
     * @return Studentgroup
     */
    public function setSessioninfos(Sessioninfos $sessioninfos): Studentgroup
    {
        $this->sessioninfos = $sessioninfos;
        return $this;
    }

    /**
     * @param Userslist $userslist
     * @return $this
     */
    public function addUsersList(Userslist $userslist) {
        $this->userslist[] = $userslist;
        return $this;
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTheme(): ?string
    {
        return $this->theme;
    }

    public function setTheme(string $theme): self
    {
        $this->theme = $theme;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @param \App\Entity\Mediaslist $mediaslist
     * @return array
     */
    public function setMediaslist(Mediaslist $mediaslist) :array
    {
        $this->mediaslist[] = $mediaslist;
        return $this;
    }
    public function getMediaslist()
    {
        return $this->mediaslist;
    }
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    public function getTimestampdebut(): ?int
    {
        return $this->timestampdebut;
    }

    public function setTimestampdebut(int $timestampdebut): self
    {
        $this->timestampdebut = $timestampdebut;

        return $this;
    }

    public function getTimestampfin(): ?int
    {
        return $this->timestampfin;
    }

    public function setTimestampfin(int $timestampfin): self
    {
        $this->timestampfin = $timestampfin;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * @param \DateTimeInterface $datedebut
     * @return $this
     */
    public function setDatedebut(\DateTimeInterface $datedebut)
    {
        $this->datedebut = $datedebut;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

    /**
     * @param \DateTimeInterface $datefin
     * @return $this
     */
    public function setDatefin(\DateTimeInterface $datefin)
    {
        $this->datefin = $datefin;
        return $this;
    }


    /**
     * @return boolean
     */
    public function getIsactive(): bool
    {
        return $this->isactive;
    }

    /**
     * @param boolean $isactive
     * @return Studentgroup
     */
    public function setIsactive(bool $isactive): Studentgroup
    {
        $this->isactive = $isactive;
        return $this;
    }



    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }


}
