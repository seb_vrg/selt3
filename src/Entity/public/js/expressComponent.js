$(function(){

	
		
		let menuItems = [{
			
			"key": "Gestion Clients",
			"items": ["Gestion Sociétés"],
			"link" : ["manageCompany"]
		},
		{
			
			"key": "Gestion Médias",
			"items": ["Stock"],
			"link" : ["manageMedias"]
		},
		{
			
			"key": "Gestion Formations",
			"items": ["Sessions Formations"],
			"link" : ["manageFormations"]
			
		}
		];

		$("#simpleList").dxList({
			dataSource: menuItems,
			height: "100%",
			grouped: true,
			collapsibleGroups: true,
			groupTemplate: function(data){
				return $("<div><a href='"+ data.link +"'>" + data.items + "</a></div>");
				}
			});
		
		
	})