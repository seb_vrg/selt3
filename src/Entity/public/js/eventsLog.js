$(function(){
	
	$(document).ready(function(){
		$("#but-deconnect").click(function(){
			$("#bloc-connection").show();
			$("#user-connected").hide();
		})
		
		$("#but-login").click(function(){
			$("#user-connected").show();
			$("#bloc-deconnect").hide();
		})
		
		$("#monBoutonCouleur").click(function(){
			$("#maBoite").show();
		})
		
		$("#monBoutonFantome").click(function(){
			$("#maBoite").hide();
		})

		$("#close-profil-ref").click(function(){
			$("#box-profil-referant").hide();
		})

		$("#add-stagiaire-for-company").click(function(){
			$("#ajout-user-for-company").show();
		})

		$("#close-register-stagiaire-for-company").click(function(){
			$("#ajout-user-for-company").hide();
		})

		$("#del-stagiaire-for-company").click(function(){
			$("#remove-user-for-company").show();
		})

		$("#btn-close-supp-stag").click(function(){
			$("#remove-user-for-company").hide();
		})

		$("input[type=file]").change(function(){
			var fileName = $(this).val();
			if(fileName != undefined || fileName != "")
			{
				$(this).next('.custom-file-label').html(fileName);
			}

		})
	})
	
})