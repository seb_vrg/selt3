<?php

namespace App\Entity;

use App\Entity\User;
use App\Entity\Questionnaire;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;


/**
 * @ORM\Table(name="repax")
 * @ORM\Entity(repositoryClass="App\Repository\RepaxRepository")
 */
class Repax
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

        /**
     * @var string
     *
     * @ORM\Column(name="pax_rep1", type="string", length=40,nullable=false)
     */
    private $paxRep1;

    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep2", type="string", length=40, nullable=true)
     */
    private $paxRep2;

    /**
     * @var string
     * 
     * @ORM\Column(name="pax_rep3", type="string", length=40, nullable=true)
     */
    private $paxRep3;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep4", type="string", length=40, nullable=true)
     */
    private $paxRep4;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep5", type="string", length=40, nullable=true)
     */
    private $paxRep5;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep6", type="string", length=40, nullable=true)
     */
    private $paxRep6;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep7", type="string", length=40, nullable=true)
     */
    private $paxRep7;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep8", type="string", length=40, nullable=true)
     */
    private $paxRep8;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep9", type="string", length=40, nullable=true)
     */
    private $paxRep9;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep10", type="string", length=40, nullable=true)
     */
    private $paxRep10;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep11", type="string", length=40, nullable=true)
     */
    private $paxRep11;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep12", type="string", length=40, nullable=true)
     */
    private $paxRep12;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep13", type="string", length=40, nullable=true)
     */
    private $paxRep13;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep14", type="string", length=40, nullable=true)
     */
    private $paxRep14;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep15", type="string", length=40, nullable=true)
     */
    private $paxRep15;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep16", type="string", length=40, nullable=true)
     */
    private $paxRep16;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep17", type="string", length=40, nullable=true)
     */
    private $paxRep17;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep18", type="string", length=40, nullable=true)
     */
    private $paxRep18;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep19", type="string", length=40, nullable=true)
     */
    private $paxRep19;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax_rep20", type="string", length=40, nullable=true)
     */
    private $paxRep20;
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="repax")
     */
    private $user;

    /**
     * @var Questionnaire
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Questionnaire", inversedBy="repax")
     */
    private $questionnaire;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setPaxRep1($paxRep1)
    {
        $this->paxRep1 = $paxRep1;

        return $this;
    }

     /**
     * Get paxRep1
     *
     * @return string
     */
    public function getPaxRep1()
    {
        return $this->paxRep1;
    }

    public function setPaxRep2($paxRep2)
    {
        $this->paxRep2 = $paxRep2;

        return $this;
    }

    /**
     * Get paxRep2
     *
     * @return string
     */
    public function getPaxRep2()
    {
        return $this->paxRep2;
    }

    public function setPaxRep3($paxRep3)
    {
        $this->paxRep3 = $paxRep3;

        return $this;
    }

     /**
     * Get paxRep3
     *
     * @return string
     */
    public function getPaxRep3()
    {
        return $this->paxRep3;
    }

    public function setPaxRep4($paxRep4)
    {
        $this->paxRep4 = $paxRep4;

        return $this;
    }

    /**
     * Get paxRep4
     *
     * @return string
     */
    public function getPaxRep4()
    {
        return $this->paxRep4;
    }

    public function setPaxRep5($paxRep5)
    {
        $this->paxRep5 = $paxRep5;

        return $this;
    }

    /**
     * Get paxRep5
     *
     * @return string
     */
    public function getPaxRep5()
    {
        return $this->paxRep5;
    }
    public function setPaxRep6($paxRep6)
    {
        $this->paxRep6 = $paxRep6;

        return $this;
    }

     /**
     * Get paxRep6
     *
     * @return string
     */
    public function getPaxRep6()
    {
        return $this->paxRep6;
    }

    public function setPaxRep7($paxRep7)
    {
        $this->paxRep7 = $paxRep7;

        return $this;
    }

    /**
     * Get paxRep7
     *
     * @return string
     */
    public function getPaxRep7()
    {
        return $this->paxRep7;
    }

    public function setPaxRep8($paxRep8)
    {
        $this->paxRep8 = $paxRep8;

        return $this;
    }

     /**
     * Get paxRep8
     *
     * @return string
     */
    public function getPaxRep8()
    {
        return $this->paxRep8;
    }

    public function setPaxRep9($paxRep9)
    {
        $this->paxRep9 = $paxRep9;

        return $this;
    }

    /**
     * Get paxRep9
     *
     * @return string
     */
    public function getPaxRep9()
    {
        return $this->paxRep9;
    }

    public function setPaxRep10($paxRep10)
    {
        $this->paxRep10 = $paxRep10;

        return $this;
    }
    /**
     * Get paxRep10
     *
     * @return string
     */
    public function getPaxRep10()
    {
        return $this->paxRep10;
    }
    public function setPaxRep11($paxRep11)
    {
        $this->paxRep11 = $paxRep11;

        return $this;
    }
    /**
     * Get paxRep11
     *
     * @return string
     */
    public function getPaxRep11()
    {
        return $this->paxRep11;
    }
    public function setPaxRep12($paxRep12)
    {
        $this->paxRep12 = $paxRep12;

        return $this;
    }
    /**
     * Get paxRep12
     *
     * @return string
     */
    public function getPaxRep12()
    {
        return $this->paxRep12;
    }
    public function setPaxRep13($paxRep13)
    {
        $this->paxRep13 = $paxRep13;

        return $this;
    }
    /**
     * Get paxRep13
     *
     * @return string
     */
    public function getPaxRep13()
    {
        return $this->paxRep13;
    }
    public function setPaxRep14($paxRep14)
    {
        $this->paxRep14 = $paxRep14;

        return $this;
    }
    /**
     * Get paxRep14
     *
     * @return string
     */
    public function getPaxRep14()
    {
        return $this->paxRep14;
    }
    public function setPaxRep15($paxRep15)
    {
        $this->paxRep15 = $paxRep15;

        return $this;
    }
     /**
     * Get paxRep15
     *
     * @return string
     */
    public function getPaxRep15()
    {
        return $this->paxRep15;
    }
    public function setPaxRep16($paxRep16)
    {
        $this->paxRep16 = $paxRep16;

        return $this;
    }
    /**
     * Get paxRep16
     *
     * @return string
     */
    public function getPaxRep16()
    {
        return $this->paxRep16;
    }
    public function setPaxRep17($paxRep17)
    {
        $this->paxRep17 = $paxRep17;

        return $this;
    }
    /**
     * Get paxRep17
     *
     * @return string
     */
    public function getPaxRep17()
    {
        return $this->paxRep17;
    }
    public function setPaxRep18($paxRep18)
    {
        $this->paxRep18 = $paxRep18;

        return $this;
    }
    /**
     * Get paxRep18
     *
     * @return string
     */
    public function getPaxRep18()
    {
        return $this->paxRep18;
    }
    public function setPaxRep19($paxRep19)
    {
        $this->paxRep19 = $paxRep19;

        return $this;
    }
    /**
     * Get paxRep19
     *
     * @return string
     */
    public function getPaxRep19()
    {
        return $this->paxRep19;
    }
    public function setPaxRep20($paxRep20)
    {
        $this->paxRep20 = $paxRep20;

        return $this;
    }
    /**
     * Get paxRep20
     *
     * @return string
     */
    public function getPaxRep20()
    {
        return $this->paxRep20;
    }

    public function setQuestionnaire(Questionnaire $questionnaire)
    {
        $this->questionnaire = $questionnaire;

        return $this;
    }

    public function getQuestionnaire()
    {
        return $this->questionnaire;
    }

    public function setUser(User $user = null)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }
}

