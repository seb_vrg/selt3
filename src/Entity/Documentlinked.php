<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="documentlinked")
 * @ORM\Entity(repositoryClass="App\Repository\DocumentlinkedRepository")
 */
class Documentlinked
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le nom est obligatoire !")
     *
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le lien est obligatoire !")
     * @Assert\Url(message="L'URL {{ value }} n'est pas une url valide !")
     */
    private $link;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeInterface
     */
    private $updateAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Mediaslist")
     * @ORM\JoinTable(name="links_list",
     *  joinColumns={@ORM\JoinColumn(name="link_id",referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="medialist_id",referencedColumnName="id")}
     * )
     */
    private $mediaslist;



    /**
     * Documentlinked constructor.
     */
    public function __construct()
    {
        $this->mediaslist = new ArrayCollection();

    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getUpdateAt(): \DateTimeInterface
    {
        return $this->updateAt;
    }

    /**
     * @param \DateTimeInterface $updateAt
     * @return Documentlinked
     */
    public function setUpdateAt(\DateTimeInterface $updateAt): Documentlinked
    {
        $this->updateAt = $updateAt;
        return $this;
    }


    /**
     * @return Collection|Mediaslist[]
     */
    public function getMediaslist(): Collection
    {
        return $this->mediaslist;
    }

    public function addMediaslist(Mediaslist $mediaslist): self
    {
        if (!$this->mediaslist->contains($mediaslist)) {
            $this->mediaslist[] = $mediaslist;
        }

        return $this;
    }

    public function removeMediaslist(Mediaslist $mediaslist): self
    {
        if ($this->mediaslist->contains($mediaslist)) {
            $this->mediaslist->removeElement($mediaslist);
        }

        return $this;
    }

}
