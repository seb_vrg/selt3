<?php

namespace App\Entity;

use App\Entity\User;
use App\Entity\Studentgroup;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="hasreferant", type="boolean", nullable=true)
     */
    private $hasreferant;


    /**
     * @var Userslist
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Userslist",mappedBy="company")
     */
    private $userslist;
    /**
     * @var User
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\User",mappedBy="company",cascade={"persist","remove"})
     */
    private $user;

    /**
     * @var Studentgroup
     * 
     * @ORM\OneToMany(targetEntity="App\Entity\Studentgroup",mappedBy="company",cascade={"persist","remove" })
     */
    private $studentgroup;

    public function __toString()
    {
        return $this->name;
    }

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->studentgroup = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return Userslist
     */
    public function getUserslist(): Userslist
    {
        return $this->userslist;
    }

    /**
     * @param Userslist $userslist
     * @return Company
     */
    public function setUserslist(Userslist $userslist): Company
    {
        $this->userslist = $userslist;
        return $this;
    }


    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getHasReferant(): ?bool
    {
        return $this->hasreferant;
    }

    public function setHasreferant(?bool $hasreferant): self
    {
        $this->hasreferant = $hasreferant;

        return $this;
    }

    public function adduser(User $user)
    {
        $this->user[] = $user;
    }

    public function removeUser(User $user)
    {
        $this->user->removeElement($user);
    }

    public function getUser()
    {
        return $this->user;
    }

    public function addStudentgroup(Studentgroup $studentgroup)
    {
        $this->studentgroup[] = $studentgroup;
    }

    public function removeStudentgroup(Studentgroup $studentgroup)
    {
        $this->studentgroup->removeElement($studentgroup);
    }

    public function getStudentgroup()
    {
        return $this->studentgroup;
    }
}
