<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email',EmailType::class)
            ->add('username')
            ->add('password',PasswordType::class)
            ->add('confirm_password',PasswordType::class)
            ->add('company',EntityType::class,[
                'class'=>Company::class,
                'label'=>'Société de rattachement'
               
            ])
            ->add('nom',TextType::class)
            ->add('prenom',TextType::class)
            ->add('isstudent',ChoiceType::class,array(
                'label'=>'L\'utilisateur est-il stagiaire ?',
                'data'=>TRUE,
                'choices'=>array(
                    'OUI'=>TRUE,
                    'NON'=>FALSE
                ),
                'multiple'=>FALSE,
                'expanded'=>TRUE
            ))
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
