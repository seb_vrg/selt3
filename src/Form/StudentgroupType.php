<?php

namespace App\Form;

use App\Entity\Studentgroup;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentgroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('theme',TextType::class,[
                'label' => "Theme de la formation",
                'required' => true,
            ])
            ->add('name',TextType::class,[
                'label' => 'Nom de la formation',
                'required' => true,

            ])
            ->add('datedebut',DateType::class,[
                'label' => 'Date de début de formation',
                'required' => true,
               'widget'=> 'single_text',
                //'format' => 'dd-MM-yyyy'
            ])
            ->add('datefin',DateType::class,[
                'label' => 'Date de fin de formation',
                'required' => true,
                'widget'=> 'single_text',
                //'format' => 'dd-MM-yyyy'
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => Studentgroup::class
        ]);
    }
}
