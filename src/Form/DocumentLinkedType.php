<?php

namespace App\Form;

use App\Entity\Documentlinked;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentLinkedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type',ChoiceType::class,[
                'choices' => [
                    'Lien PDF' => 'pdflink',
                    'LIEN VIDEO' => 'videolink'
                ],
                'label' => 'Types'
            ])
            ->add('name',TextType::class,[
                'required' => true,
                'label' => 'Nom du média'
            ])

            ->add('link',TextType::class,[
                'required' => true,
                'label' => 'URL du lien vidéo'
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Documentlinked::class,
        ]);
    }
}
