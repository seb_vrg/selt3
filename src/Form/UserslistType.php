<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\User;
use App\Entity\Userslist;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserslistType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('namelist',TextType::class,[
                'label' => 'Nom de la liste'
            ])
            ->add('company',EntityType::class,[
                'class'=> Company::class,
                'label' => 'Pour quelle Société ?',
                'choice_label'=> 'name',
                'required' =>true
            ])

//            ->add('user',EntityType::class,[
//               'class'=> User::class,
//                'label'=>'Utilisateurs',
//                'choice_label'=> 'nom',
//                'required' => false
//            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Userslist::class,
        ]);
    }
}
