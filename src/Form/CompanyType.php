<?php

namespace App\Form;

use App\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;




class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class)
            ->add('hasreferant',ChoiceType::class,array(
                'required'=>TRUE,
               'choices'=>array(
                   'OUI'=>TRUE,
                   'NON'=>FALSE
               ),
                'label'=> 'Ajouter un référant',
                'label_attr'=>array(
                    'class'=>'form-check-label'
                ),
                'attr'=>array(
                    'class'=>'form-check add-ref'
                )
            ))
          
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
