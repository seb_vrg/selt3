<?php
/**
 * Created by PhpStorm.
 * User: SebLoc
 * Date: 11/06/2019
 * Time: 15:38
 */

namespace App\Formations;


interface InfosDelaiFormation
{

    /**
     * Donne le chrono entre la date du jour et le début de formation
     * @param int $delai
     * @param int $stampDateFin
     * @return string
     */
    public function getDelaiFormation(int $delai,int $stampDateFin) : string;
}