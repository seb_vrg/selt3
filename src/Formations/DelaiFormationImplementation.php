<?php
/**
 * Created by PhpStorm.
 * User: SebLoc
 * Date: 11/06/2019
 * Time: 15:44
 */

namespace App\Formations;


class DelaiFormationImplementation implements InfosDelaiFormation
{

    /**
     * Donne le chrono entre la date du jour et le début de formation
     * @param int $delai
     * @param int $stampDateFin
     * @return string
     */
    public function getDelaiFormation(int $delai, int $stampDateFin): string
    {
        if ($delai < 0) {
            $delai_restant = "La session est ouverte !";

            //Verification si la date de fin n'est pas dépassée
            $dateDuJour = time();
            $verifDepasse = $dateDuJour - $stampDateFin;

            if ($verifDepasse > 0) {
                $delai_restant = "La session est terminée !";
            }
        } else {
            $stampDateFin = null;
            $delai_copy = $delai;

            $temps = array(1);

            array_unshift($temps, 60*$temps[0]);
            array_unshift($temps,60*$temps[0]);
            array_unshift($temps,24*$temps[0]);

            $temps = array_combine(array('Jour','Heure','Minute','Seconde'),$temps);
            $delai_restant = '';

            foreach ($temps as $unit=>$format)
            {
                $count = number_format($delai_copy / $format,0);
                $delai_copy %= $format;
                if ($count or $format == 1)
                    $delai_restant .= " $count $unit" .(($count > 1) ? 's': '');

            }
            $delai_restant = trim($delai_restant);
        }
        return $delai_restant;
    }
}