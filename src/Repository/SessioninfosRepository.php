<?php

namespace App\Repository;

use App\Entity\Sessioninfos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Sessioninfos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sessioninfos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sessioninfos[]    findAll()
 * @method Sessioninfos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SessioninfosRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Sessioninfos::class);
    }

    // /**
    //  * @return Sessioninfos[] Returns an array of Sessioninfos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sessioninfos
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
