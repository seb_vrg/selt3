<?php

namespace App\Repository;

use App\Entity\Repax;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


class RepaxRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parrent::__construct($registry, Repax::class);
    }
}