<?php

namespace App\Repository;

use App\Entity\Studentgroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Studentgroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method Studentgroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method Studentgroup[]    findAll()
 * @method Studentgroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentgroupRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Studentgroup::class);
    }

    // /**
    //  * @return Studentgroup[] Returns an array of Studentgroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Studentgroup
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
