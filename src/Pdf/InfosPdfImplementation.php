<?php

namespace App\Pdf;

use App\Entity\Documentpdf;
use App\Pdf\StatutUploadInterface;

class InfosPdfImplementation implements StatutUploadInterface
{
    /**
     * Confirmation of uploading pdf
     *
     * @param Documentpdf $pdf
     * @return string
     */
   public function getInfosUploadPdf(Documentpdf $pdf) :string 
   {
       $unite = [' octet',' Ko',' Mo',' Go'];
       $pdfSize = $pdf->getDocumentSize();

       if ($pdfSize < 1000) {
           $poids = $pdfSize.$unite[0];                     //octet
       } else {
           if ($pdfSize < 1000000) {                        //ko
               $ko = round($pdfSize/1024,2);

               $poids = $ko.$unite[1];
           } else {
               if ($pdfSize < 1000000000) {                 //Mo
                   $mo = round($pdfSize/(1024*1024),2);
                   $poids = $mo.$unite[2];
               } else {                                     //Go
                   $go = round($pdfSize/(1024*1024*1024),2);
                   $poids = $go.$unite[3];
               }
           }
       }

       $pdfName = $pdf->getName();
       $message = 'Nom : '.$pdfName.PHP_EOL.'- Poids : '.$poids;

        return $message;

   } 
}