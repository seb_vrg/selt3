<?php

namespace App\Pdf;

use App\Entity\Documentpdf;

interface StatutUploadInterface
{

    /**
     * Infos on the pdf document uploaded
     *
     * @param Documentpdf $id
     * @return string
     */
    public function getInfosUploadPdf(Documentpdf $pdf) :string;
  
}


