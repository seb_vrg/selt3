<?php

namespace App\Controller;

use App\Entity\Studentgroup;
use App\Entity\User;
use App\Entity\Userslist;
use App\Form\StudentgroupType;
use App\Form\UserslistType;
use App\Formations\DelaiFormationImplementation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class FormationsController extends AbstractController
{
    /**
     * @Route("/formations/home", name="formations_home")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function index()
    {
        return $this->render('formations/index.html.twig', [

        ]);
    }

    /**
     * @Route("/formations/createList",name="create_list")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createUsersList(Request $request,EntityManagerInterface $manager)
    {
        $listStudents = new Userslist();

        $form = $this->createForm(UserslistType::class,$listStudents);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $nameList = $listStudents->getNamelist();
            $companyName = $listStudents->getCompany()->getName();

            $manager->persist($listStudents);
            $manager->flush();
            $this->addFlash('info',"<p class='text-center'> La liste <b>$nameList</b> pour la société <b>$companyName</b> a été crée.<br><cite>Vous pouvez la rattachée à une session existante.</cite></p>");
            $idUsersList = $listStudents->getId();
           return $this->redirectToRoute('create_group',[
               'id' => $idUsersList

           ]);


        }
        return $this->render('formations/create_list.html.twig',[
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/formations/showUserslist",name="show_userslist")
     * @param Userslist $userslist
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showUsersList(Userslist $userslist)
    {
        return $this->render('formations/show_userslist.html.twig');

    }

    /**
     * @Route("/formations/createGroup/{id}",defaults={"id":null},name="create_group")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createGroup(Request $request,EntityManagerInterface $manager,$id)
    {
        $sessionGroup = new Studentgroup();
        //if $id is  null ->  createGroup sans Userslist
        if (is_null($id)) {

            //Formulaire Group (session) sans userslist
            $userslist = null;
            $companyName = null;
        } else {
            // Formulaire lié à l'userslist
           $userslist = $manager->find(Userslist::class,$id);
           $usersListName = $userslist->getNamelist();
            $companyList = $userslist->getCompany();
            $companyName = $userslist->getCompany()->getName();
            $hasGroup = $userslist->getStudentgroup(); // A voir
            $formGroup  = $this->createForm(StudentgroupType::class,$sessionGroup);
            $formGroup->handleRequest($request);

            if ($formGroup->isSubmitted() && $formGroup->isValid()) {


                $groupName= $sessionGroup->getName();

                $dateDebut = $sessionGroup->getDatedebut();
                $dateFin = $sessionGroup->getDatefin();



                $stampDebut = $dateDebut->getTimestamp();
                $stampFin = $dateFin->getTimestamp();

                $stampToday = time();
                $delai = $stampDebut - $stampToday;

                $delaiInfos = new DelaiFormationImplementation();
                //INFO Délai avant début de formation
                $delai_restant = $delaiInfos->getDelaiFormation($delai,$stampFin);


                $sessionGroup->addUsersList($userslist);
                $sessionGroup->setCompany($companyList);
                $sessionGroup->setDatedebut($dateDebut);
                $sessionGroup->setDatefin($dateFin);
                $sessionGroup->setIsactive(false);
                $sessionGroup->setTimestampdebut($stampDebut);
                $sessionGroup->setTimestampfin($stampFin);

                $manager->persist($sessionGroup);
                $manager->flush();
               /* TODO : faire vérification erreur si pas flush */
                if (null != $sessionGroup->getId() ) {

                    $userslist->setStudentgroup($sessionGroup);
                    $manager->flush();
                }

                $this->addFlash('success',"<p class='text-center'>Le groupe <b>$groupName</b> à bien été crée.<br><cite>Vous pouvez désormais ajouter des participants à la liste <b>$usersListName</b> que vous avez crée.</cite><br>La session débute dans : $delai_restant</p>");
                return $this->redirectToRoute('add_users',[
                    'id' => $id
                ]);

            }
        }

        return $this->render('formations/create_session_group.html.twig',[
            'form' => $formGroup->createView(),
            'list' => $userslist,
            'company' => $companyName
        ]);
    }


    /**
     * @Route("/formations/addUsersInList/{id}",name="add_users")
     * @param Request $request
     * @param Userslist $userslist
     * @param EntityManagerInterface $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addUsersInUserslist(Request $request,Userslist $userslist,EntityManagerInterface $manager)
    {
        $idCompany = $userslist->getCompany()->getId();
        $companyName = $userslist->getCompany()->getName();
         $groupName = $userslist->getStudentgroup()->getName();
        $nameUserList = $userslist->getNamelist();
        $nbUser = $userslist->getUser();

        $repository = $this->getDoctrine()->getRepository(User::class);

        $companyList = $repository->findBy([
            'company' => $idCompany
        ],[
            'nom' =>'ASC'
        ]);
        if ($request->isMethod('POST')) {
            
        }

        dump($companyList);

        return $this->render('formations/add_users_in_userslist.html.twig',[
            'companyUsers' => $companyList,
            'companyName' => $companyName,
            'nameList' => $nameUserList,
            'groupName' => $groupName
        ]);


    }
}
