<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Company;
use App\Form\CompanyType;
use App\Form\RegistrationType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AgencyController extends AbstractController
{
    /**
     * @Route("/agency", name="agency_home")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function index()
    {
            $user= $this->getUser();
            
           

        return $this->render('agency/index.html.twig', [
            'controller_name' => 'AgencyController',
            'user'=> $user
        ]);
    }

    /**
     * @Route("/agency/managecompany",name="manageCompany")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function manageCompany()
    {
        return $this->render('agency/manage_company.html.twig');
    }

    /**
     * @Route("agency/manageUsers",name="manageUsers")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function manageUsers()
    {
        return $this->render('agency/manage_users.html.twig');
    }

    /**
     * @Route("/agency/addCompany", name="addCompany")
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addCompany(Request $request, ObjectManager $manager)
    {
        $company = new Company;

        $form= $this->createForm(CompanyType::class,$company);
        $form->handleRequest($request);
            $data=null;
        if ($form->isSubmitted() && $form->isValid()) {

           $data=$request->request->all();
            $datacie=$data['company'];
            $companyName=$datacie['name'];
            $company->setHasReferant(FALSE);
            $manager->persist($company);
            $manager->flush();

           if ($datacie['hasreferant']) {
              
               $dataref= "Ajouter un référant : oui, société: $companyName";
             
               return $this->redirectToRoute('addReferant',[
                   'company'=>$company->getId()
               ]);
           }else{
               $dataref= "Ajouter un référant : non, société: $companyName";

               return $this->redirectToRoute('listCompany');
           }
           
        }

        return $this->render('agency/add_company.html.twig',[
            'form'=> $form->createView(),
            'data'=>$data
        ]);
    }

    /**
     * @Route("/agency/addReferant/{company}",name="addReferant")
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param Request $request
     * @param Company $company
     * @param ObjectManager $manager
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addReferantInCompany(Request $request,Company $company,ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {

        $referant= new User;
        $form= $this->createForm(RegistrationType::class,$referant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) 
        {
            $hash= $encoder->encodePassword($referant, $referant->getPassword());
            $referant->setPassword($hash);
            $referant->setRoles(array('ROLE_ADMIN'));
            $referant->setIsReferant(TRUE);
            $referant->setCompany($company);
            $company->setHasReferant(TRUE);

            $manager->persist($referant);
            $manager->flush();

            return $this->redirectToRoute('listCompany');

        }

        return $this->render('agency/add_referant.html.twig',array(
            'nomCompany'=>$company->getName(),
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/agency/listCompany",name="listCompany")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function listCompany()
    {
        $repoCommpany=$this->getDoctrine()->getRepository(Company::class);
        $list=$repoCommpany->findAll();

        $repoUser=$this->getDoctrine()->getRepository(User::class);
        $listuser=$repoUser->findAll();

        $viewProfil=FALSE;

        return $this->render('agency/list_company.html.twig',[
            'list'=>$list,
            'listuser'=>$listuser,
            'viewProfilRef'=>$viewProfil
        ]);
    }

    /**
     * @Route("/agency/viewProfileReferant/{company_ref}",name="viewProfilRef")
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param Company $company_ref
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewProfileReferant(Company $company_ref)
    {

        $viewProfil=TRUE;

        $repoCommpany=$this->getDoctrine()->getRepository(Company::class);
        $list=$repoCommpany->findAll();

       
        $repoUserRef=$this->getDoctrine()->getRepository(User::class);
        $profilRef=$repoUserRef->findOneBy(array(
            'company'=>$company_ref,
            'isreferant'=>TRUE
            
        ));

        return $this->render('agency/list_company.html.twig',[
            'viewProfilRef'=>$viewProfil,
            'societe_user'=>$company_ref,
            'list'=>$list,
            'profilRef'=>$profilRef
        ]);
    }


    /**
     * @Route("/agency/listStudentCompany/{company}",name="listStudCompany")
     * @param Company $company
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function listStudentForCompany(Request $request,Company $company,ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        $stagiaire= new User;
       
        
       
        /* FORM AJOUTER STAGIAIRE  */
        $form= $this->createFormBuilder(null,array(
            'csrf_protection' => true,
           'allow_extra_fields' => true,
        ))
        ->add('username',TextType::class,array(
            'allow_extra_fields' => true,
            'csrf_protection' => FALSE,
            'label'=>'Pseudo',
            'required'=>TRUE,
            'attr'=>array(
                'placeholder'=>'pseudo stagiaire',
                'class'=>'form-control form-control-sm'
            )          
        ))
       ->add('email',EmailType::class,array(
        'allow_extra_fields' => true,
        'csrf_protection' => FALSE,
        'required'=>TRUE,
        'label'=>'Email',
        'attr'=>array(
            'placeholder'=>'email stagiaire',
            'class'=>'form-control form-control-sm'
        )   

       ))
        ->add('nom',TextType::class,array(
            'allow_extra_fields' => true,
            'csrf_protection' => FALSE,
            'label'=>'Nom',
            'required'=>TRUE,
            'attr'=>array(
                'placeholder'=>'nom stagiaire',
                'class'=>'form-control form-control-sm'
            )        
        ))
        ->add('prenom',TextType::class,array(
            'allow_extra_fields' => true,
            'csrf_protection' => FALSE,
            'label'=>'Prénom',
            'required'=>TRUE,
            'attr'=>array(
                'placeholder'=>'prenom stagiaire',
                'class'=>'form-control form-control-sm'
            )        
        ))
        ->add('password',PasswordType::class,array(
            'allow_extra_fields' => true,
            'label'=>'Password',
            'required'=>TRUE,
            'attr'=>array(
                'placeholder'=>'mot de passe 8 carract. mini.',
                'class'=>'form-control form-control-sm'
            )
        ))
        ->add('confirm_password',PasswordType::class,array(
            'allow_extra_fields' => true,
            'label'=>'Confirmez le password',
            'required'=>TRUE,
            'attr'=>array(
                'placeholder'=>'password',
                'class'=>'form-control form-control-sm'
            )
        ))
        ->add('submit',SubmitType::class,array(
            'label'=>'Ajouter',
            'attr'=>array(
                'class'=>'form-control btn btn-success'
            )
        ))
        ->getForm()
        ;
        $form->handleRequest($request);
        $formView=$form->createView();


        $companyName=$company->getName();

        $usersRepo=$this->getDoctrine()->getRepository(User::class);
        $usersList=$usersRepo->findBy(array(
            'company'=>$company
        ));

    
        if($form->isSubmitted())
        {
            $dataForm=$request->get('form');

            if ($dataForm['password'] != $dataForm['confirm_password']) {
               $error_form=TRUE;
               $error_message="Les mots de passes doivent être identiques !";
            }
            else
            {
                $error_form=FALSE;
                $error_message=null;
                $hash=$encoder->encodePassword($stagiaire, $dataForm['password']);
                $stagiaire->setUsername($dataForm['username']);
                $stagiaire->setEmail($dataForm['email']);
                $stagiaire->setNom($dataForm['nom']);
                $stagiaire->setPrenom($dataForm['prenom']);
                $stagiaire->setRoles(array('ROLE_USER'));
                $stagiaire->setCompany($company);
                $stagiaire->setIsReferant(FALSE);
                $stagiaire-> setIsStudent(TRUE);
                $stagiaire->setPassword($hash);

                $manager->persist($stagiaire);
                $manager->flush();

                return $this->redirectToRoute('listStudCompany',[
                    'company'=>$company->getId()
                ]);

            }
        }
        else
        {
            $dataForm=null;
            $error_form=FALSE;
            $error_message=null;

        }


        return $this->render('agency/list_student_company.html.twig',[
            'listUsers'=>$usersList,
            'nomCompany'=>$companyName,
            'form'=>$formView,
            'dataForm'=>$dataForm,
            'errorForm'=>$error_form,
            'errorMessage'=>$error_message,
            'companyId'=>$company->getId()
        ]);
    }

    /**
     * @Route("/agency/removeStudent/{company}",name="removeStudentFromCompany")
     * @param Company $company
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteStagiaireFromCompany(Request $request,Company $company,ObjectManager $manager)
    {

    
            $stagiaireId=$request->request->get('stagiaireId');
            $repoStagiaire=$this->getDoctrine()->getRepository(User::class);
            $stagiaire=$repoStagiaire->find($stagiaireId);
            $manager->remove($stagiaire);
            $manager->flush();
       
        return $this->redirectToRoute('listStudCompany',[
           'company'=>$company->getId()
       
        ]);
    }
}
