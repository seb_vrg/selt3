<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\RegistrationType;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;



class HomeController extends AbstractController
{
   
    /**
     * @Route("/",name="accueil")
     * 
     */
    public function homeAction(){
        $role_user = $this->isGranted('ROLE_USER');
        $role_customer = $this->isGranted('ROLE_ADMIN');
        $role_agence= $this->isGranted('ROLE_SUPER_ADMIN');

        if ($role_user){
            $role= "Stagiaire";
        }
        if($role_customer){
            $role= "Administrateur client";
        }
        if($role_agence){
            $role= "Administrateur Agence";

            return $this->redirectToRoute('agency_home');
        }

        if (!$role_user && !$role_agence && !$role_customer) {
           $role = "Utilisateur NON CONNECTE";
        }


        return $this->render('home/home.html.twig', [
            'title' => "ACCUEIL",
            'role'=>$role
        ]);
    }
    
    /**
     * @Route("/inscription",name="security_registration")
     */
    public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder){
        
        $user= new User();
        
        $form= $this->createForm(RegistrationType::class,$user);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            $hash= $encoder->encodePassword($user, $user->getPassword());
            
            $user->setPassword($hash);
            $user->setIsReferant(FALSE);
            $manager->persist($user);
            $manager->flush();
            
            return $this->redirectToRoute('security_login');
            
        }
        
        return $this->render('home/security/registration.html.twig',[
            'form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/login",name="security_login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
  
        $error = $authenticationUtils->getLastAuthenticationError();
       $lastUsername = $authenticationUtils->getLastUsername(); 

        return $this->render('home/security/login.html.twig',[
            'last_username'=>$lastUsername,
            'error'=>$error,
           
        ]);
            
        
            
    }
    
    /**
     * @Route("/logout",name="security_logout")
     */
    public function logout(){}

   

   

  
}
